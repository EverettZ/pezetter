import { Component } from '@angular/core';
import { ResumeService } from './services/resume/resume.service';

@Component({
  selector: 'pez-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {}

}
